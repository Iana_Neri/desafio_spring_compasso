package com.desafio.java.springboot.desafio.java.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.desafio.java.springboot.desafio.java.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {
	
	Optional<Product> findById(String id);
		
	@Query("select s from Product s "
	        + "where (:q is null or s.description = :q) "
	        + "and (:q is null or s.name = :q)"
	        + "and (:min_price is null or s.price >= :min_price)"
	        + "and (:max_price is null or s.price <= :max_price)")
	List<Product> findByParams(@Param("q") Optional<String> name, @Param("min_price") Optional<Double> min_price, @Param("max_price") Optional<Double> max_price);
	
	boolean existsByid(String id);

}
