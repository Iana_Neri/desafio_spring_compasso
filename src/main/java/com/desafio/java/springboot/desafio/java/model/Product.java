package com.desafio.java.springboot.desafio.java.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Positive;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Product {
	
	@Column(unique=true, nullable=false)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	public String id;
	@Column(nullable=false)
	public String name;
	@Column(nullable=false)
	public String description;
	@Column(nullable=false)
	@Positive
	public Double price;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		if (price < 0) {
	        throw new IllegalArgumentException("O preço deve ser um número positivo.");
		}
		this.price = price;
	}

}
