package com.desafio.java.springboot.desafio.java.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.desafio.java.springboot.desafio.java.model.Product;
import com.desafio.java.springboot.desafio.java.repository.ProductRepository;

@RestController
@RequestMapping("/products")
public class ProductController {
	
	@Autowired
	private ProductRepository productRepository;
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Product addProduct(@Valid @RequestBody Product produto) {
		return productRepository.save(produto);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity updateProduct(@PathVariable String id, @Valid @RequestBody Product product) {
		Optional<Product> currentProduct = productRepository.findById(id);
		
		if (!currentProduct.isPresent()) {
			return new ResponseEntity("Erro: Produto não encontrado", HttpStatus.NOT_FOUND);
		}

		Product getProduct = currentProduct.get(); 
		BeanUtils.copyProperties(product, getProduct, "id");
		productRepository.save(getProduct);
		return new ResponseEntity("OK", HttpStatus.OK);
		 		 
	}
		
	@GetMapping("/{id}")
	public ResponseEntity showProduct(@PathVariable String id) {
		Optional<Product> product = productRepository.findById(id);
		
		if(!product.isPresent()) {
			return new ResponseEntity("Erro: Produto não encontrado", HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity(product, HttpStatus.OK);
	}
	
	@GetMapping
	public List<Product> showAllProducts() {
		return productRepository.findAll();
	}
	
	@GetMapping("/search")
	public List<Product> showProductsByParams(@RequestParam Optional<String> q, @RequestParam Optional<Double> min_price, @RequestParam Optional<Double> max_price) {
		return productRepository.findByParams(q,min_price, max_price);
	}
			
	@DeleteMapping("/{id}")
	public ResponseEntity deleteProduct(@PathVariable String id) {
		
		Optional<Product> product = productRepository.findById(id);
		
		if(!product.isPresent()) {
			return new ResponseEntity("Erro: Produto não encontrado", HttpStatus.NOT_FOUND);
		}
		
		productRepository.deleteById(id);
		return new ResponseEntity("OK", HttpStatus.OK);
		
	}
			
};
