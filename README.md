Catálogo de produtos

O objetivo é implementar um catálogo de produtos com Java e Spring Boot.

product-ms

Neste microserviço é possível criar, alterar, visualizar e excluir um determinado produto, além de visualizar a lista de produtos atuais disponíveis. Também é possível realizar a busca de produtos filtrando por name, description e price.

Endpoints

![picture](src/img/endpoints.PNG)